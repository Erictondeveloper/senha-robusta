# Senha-Robusta

# PROJETO INTEGRADO UNINORTE - SENHA ROBUSTA

# GUIA DE INSTALAÇÃO DA API

# PRÉ REQUISITOS

# MYSQL WORKBENCH
# NODEJS


# GUIA

- Executar o comando "npm install" para instalar as dependencias;
- Criar um novo usuário no Workbench com todas as permissões;
- Criar um cópia do arquivo ".env.simple" e renomear para ".env";
- Criar um novo SCHEMA no workbench com o nome que desejar;
- Preencher os dados do arquivo ".env" com o host, user, password e database do Workbench;
- Executar o comando "npm run knex:migrate" para criar as tabelas no banco de dados;
- Executar o comandoo "npm run knex:seed" para preencher dados da tabela;
- Executar o comando "npm run dev" para executar a API;


# OBSERVAÇÕES

- As rotas estão em "routes.ts";
