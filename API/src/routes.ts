import express from 'express';

const routes = express.Router();

import SignInController from './app/controllers/signIn';
import SignUpController from './app/controllers/signUp';

const signUpController = new SignUpController();
const signInController = new SignInController();

routes.post('/user/signIn', signInController.show);
routes.post('/user/signUp', signUpController.create);

export default routes;